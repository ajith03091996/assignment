require 'pry' #table print gem is for create a table view in terminal
require 'table_print' #table print gem is for create a table view in terminal
#constant variable - error messages 
NO_PARCEL = "No parcel slots available.use 'create_parcel_slot' this command to create a slot" 
NOT_VALID = "Please enter valid input"
NOT_FREE = "Sorry, parcel slot is full"
NOT_FOUND = "Not Found"

#global variable to check the error
$has_no_error = false

class Session
    
    
    def start

        puts 'Is Input type file (y/n)?'
        #get input from user
        isFile = gets.chomp 
        
        if(isFile=='y')
            InputFile.new().read
        else
            #nil command send because user needs to enter own command
            CommandPrompt.new().get_shell_command command:nil 
        end
    end

end

class InputFile

    def read
        #read the input.txt file line one by one and send it to command prompt
        File.open('input.txt').each do |command_line| 
            print "Input - #{command_line}"
            #pass command line one by one from the file
            CommandPrompt.new().get_shell_command command:command_line 
        end
    end

end

class CommandPrompt

    def get_shell_command command:command_line
        if(command==nil) 
            puts 'input' 
            command_text = gets.chomp.split(' ') 
        else
            command_text = command.split(' ')
        end

        case command_text[0]
        when "create_parcel_slot"

            number_of_lot = command_text[1]
            #create a parking lot with given number
            $has_no_error = self.create_parking_slot(number_of_lot.to_i) if(self.number_check(number_of_lot)) 

        when "park"

            registration_no = command_text[1]
            weight = command_text[2]
            #weight must be a number
            $has_no_error = self.assign_value_to_free_slot(registration_no,weight.to_i) if(self.number_check(weight)) 

        when "status"
            #show table
            $has_no_error = self.show_status()  

        when "leave"

            slot_no = command_text[1].to_i
            #to remove assigned space by given number
            $has_no_error = self.remove_assigned_value(slot_no) 

        when "parcel_code_for_parcels_with_weight"

            weight = command_text[1]
            #filter registration number by given weight
            $has_no_error = self.filter(weight.to_i,'weight','registration_no') if(self.number_check(weight)) 

        when "slot_numbers_for_parcels_with_weight"

            weight = command_text[1]
            #filter slot number by given weight
            $has_no_error = self.filter(weight.to_i,'weight','slot_no') if(self.number_check(weight)) 

        when "slot_number_for_registration_number"

            slot_no = command_text[1].to_i
            #filter registration number by given slot number
            $has_no_error = self.filter(slot_no,'registration_no','slot_no')

        when "exit"
            #dummy text to stop getting command from user
            command='not nil' 

        else
            #if any input is wrong we need to show information text
            puts 'please give valid command'

        end
        #get command from user continiously unless user put exit
        self.get_shell_command command:nil if(command==nil) 

    end

    def create_parking_slot(number_of_lot)

        rack_count = number_of_lot/2
        $total_slot = []

        rack_count.times do |number|

            slot_number = number+1
            self.create_slot_by_slot_number(slot_number)
            self.create_slot_by_slot_number(rack_count*2-number)  

        end
        puts "Created a parcel slot with #{number_of_lot} slots"
        return true
        
    end
    #create a slot by given number
    def create_slot_by_slot_number(slot_number)
        
        slot_details = {
            free: false,
            slot_no: slot_number,
            registration_no: '',
            weight: ''
        }
        $total_slot << slot_details

    end
    #assign given value to free slot
    def assign_value_to_free_slot(registration_no, weight)
        
        result = false
        if($total_slot.nil?)
            puts NO_PARCEL
        else

            slot_number = self.find_free_slot
            unless(slot_number.nil?)
                $total_slot = $total_slot.map { |single_slot| single_slot[:slot_no]==slot_number ? single_slot = {
                    free: true,
                    slot_no: slot_number,
                    registration_no: registration_no,
                    weight: weight
                } : single_slot }
                puts "Allocated slot number: #{slot_number}"
                result = true #true result only if there is no error
            end
        end
        #return true if there is no error
        return result

    end
    #find which slot number is free
    def find_free_slot
        free_slot = $total_slot.select { |single_slot| (single_slot[:registration_no] == '' || single_slot[:weight] == '') }
                               .first unless $total_slot.nil?
        puts NOT_FREE if free_slot.nil?
        #return slot number when free slot is available
        return free_slot[:slot_no] unless free_slot.nil?

    end
    #remove assigned value for the given slot number
    def remove_assigned_value(slot_no)
       
        result = false
        if $total_slot.nil?
              #return no parcel found error message if there is no slot available
              puts NO_PARCEL
        else
          $total_slot = $total_slot.map { |single_slot| single_slot[:slot_no]==slot_no ? single_slot = {
              free: false,
              slot_no: slot_no,
              registration_no: '',
              weight: ''
          } : single_slot }
          result = true
        end
        #return true if there is no error
        return result

    end
    #show current table 
    def show_status
        result = false
        unless($total_slot.nil?)
            total_slot = $total_slot
            #remove free field in table view
            total_slot.select { |single_slot| single_slot.delete('free'.to_sym) }
            tp total_slot #tp is used for create table using table_print gem
            result = true
        end
        #return true if there is no error
        return result
    end
    #filter a field by given value
    def filter(value_to_be_check, field_to_be_check, field_to_be_display)
        
        result = false
        if $total_slot.nil?
          puts NO_PARCEL
        else
          total_slot = $total_slot
          slots = total_slot.select { |single_slot| single_slot[field_to_be_check.to_sym].to_i==value_to_be_check }
                            .map { |single_slot| single_slot[field_to_be_display.to_sym] }
                            .join(",")
          if slots.empty? then puts NOT_FOUND else puts slots end
          result = true
        end
        return result

    end
    #check the string only contains number or not
    def number_check(value_to_check) 
        result = false
        if(value_to_check.nil?)
            puts NOT_VALID
        else
            result = value_to_check.scan(/\D/).empty?
            puts NOT_VALID if(result==false)
        end
        #return true if there is no error
        return result
    end

end