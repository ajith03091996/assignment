require 'rspec'
require './courier_parking_slot_assignment.rb'


describe CommandPrompt do 
    context '-> Testing parking slot -> ' do 
       
       it "create a parcel slot" do 
          CommandPrompt.new.get_shell_command command:"create_parcel_slot 6"
          expect($has_no_error).to eq true
       end
       
       it "assign value to slot" do 
          CommandPrompt.new.get_shell_command command:"park 83274 400"
          expect($has_no_error).to eq true
       end
       
       it "assign value to slot" do 
          CommandPrompt.new.get_shell_command command:"park 983 239"
          expect($has_no_error).to eq true
       end
       
       it "assign value to slot" do 
          CommandPrompt.new.get_shell_command command:"park 82374 400"
          expect($has_no_error).to eq true
       end
       
       it "assign value to slot" do 
          CommandPrompt.new.get_shell_command command:"park 3141 2378"
          expect($has_no_error).to eq true
       end
       
       it "remove assigned slot" do 
          CommandPrompt.new.get_shell_command command:"leave 5 for deliver"
          expect($has_no_error).to eq true
       end
       
       it "show table" do 
          CommandPrompt.new.get_shell_command command:"status"
          expect($has_no_error).to eq true
       end
       
       it "assign value to slot" do 
          CommandPrompt.new.get_shell_command command:"park 888 999"
          expect($has_no_error).to eq true
       end
       
       it "remove data from slot" do 
          CommandPrompt.new.get_shell_command command:"leave 5 for deliver"
          expect($has_no_error).to eq true
       end
       
       it "filter value" do 
          CommandPrompt.new.get_shell_command command:"parcel_code_for_parcels_with_weight 4d00"
          expect($has_no_error).to eq true
       end
       
       it "filter value" do 
          CommandPrompt.new.get_shell_command command:"slot_numbers_for_parcels_with_weight 400d"
          expect($has_no_error).to eq true
       end
       
       it "filter value" do 
          CommandPrompt.new.get_shell_command command:"slot_number_for_registration_number 3141"
          expect($has_no_error).to eq true
       end
       
    end
 end